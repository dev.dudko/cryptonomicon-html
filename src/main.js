import { createApp } from "vue";
import App from "./App.vue";

if (window.SharedWorker) {
  const sharedWorker = new SharedWorker('@/sharedWorkers.js');
  sharedWorker.port.start();
  window.sharedWorker = sharedWorker;
  sharedWorker.port.postMessage('test');

  createApp(App).mount("#app");
}
