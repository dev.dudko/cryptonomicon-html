const API_KEY = 'd03720aa650893e3931765a0538e97cee8d5b369cf439ee876d441e685d46fd0';
const API_POINT = 'wss://streamer.cryptocompare.com/v2';

const API_ALL_TICKERS = 'https://min-api.cryptocompare.com/data/all/coinlist?summary=true';
const AGGREGATE_INDEX = "5";
const INVALID_SUB_INDEX = "500";
const INVALID_SUB_MESSAGE = "INVALID_SUB";

let BTCPrice = 0;

const tickersHandlers = new Map();
const tickersBtcPrice = new Map();
const socket = new WebSocket(`${API_POINT}?api_key=${API_KEY}`);

socket.addEventListener('message', (event) => {
  const {
    TYPE: type,
    FROMSYMBOL: currency,
    TOSYMBOL: to,
    PRICE: newPrice,
    PARAMETER: params,
    MESSAGE: msg
  } = JSON.parse(event.data)
  if (type === INVALID_SUB_INDEX && msg === INVALID_SUB_MESSAGE) {
    debugger
    processCurrencyToBTC(params, newPrice);
    return;
  }

  if (to === 'BTC') {
    tickersBtcPrice.set(currency, newPrice);

    const handlers = tickersHandlers.get(`${currency}-BTC`) ?? [];
    handlers.forEach((handler) => handler());
  }

  if (type !== AGGREGATE_INDEX || newPrice === undefined) {
    return;
  }

  const handlers = tickersHandlers.get(`${currency}-USD`) ?? [];
  handlers.forEach((handler) => handler(newPrice));
});

function processCurrencyToBTC (params) {
  const currency = params.split(`~`)[2].split('-')[0];
  const to = params.split(`~`)[3];

  if (to === 'BTC') {
    unsubscribeFromTicker(currency, 'BTC');
    unsubscribeFromTicker(currency, 'USD');
    return;
  }

  unsubscribeFromTickerOnWebSocket(currency, to);

  subscribeToTicker('BTC', 'USD', (price) => {
    BTCPrice = price;

    const handlers = tickersHandlers.get(`${currency}-BTC`) ?? [];
    handlers.forEach((handler) => handler());
  });

  subscribeToTicker(currency, 'BTC', () => {
    const price = tickersBtcPrice.get(currency);
    const handlers = tickersHandlers.get(`${currency}-USD`) ?? [];

    const priceToUSD = price * BTCPrice;

    handlers.forEach((handler) => {
      handler(priceToUSD)
    });
  });
}

function sendToWebSocket (message) {
  const formatMessage = JSON.stringify(message);

  if (socket.readyState === WebSocket.OPEN) {
    socket.send(formatMessage);
    return;
  }

  socket.addEventListener(
    "open",
    () => {
      socket.send(formatMessage);
    },
    { once: true }
  );
}

function subscribeToTickerOnWebSocket(ticker, to) {
  sendToWebSocket({
    action: "SubAdd",
    subs: [`5~CCCAGG~${ticker}~${to}`]
  });
}

function unsubscribeFromTickerOnWebSocket(ticker, to) {
  sendToWebSocket({
    action: "SubRemove",
    subs: [`5~CCCAGG~${ticker}~${to}`]
  });
}

export const subscribeToTicker = (ticker, to, cb) => {
  const handlerKey = `${ticker}-${to}`;
  const subscribers = tickersHandlers.get(handlerKey) || [];

  tickersHandlers.set(handlerKey, [...subscribers, cb]);
  subscribeToTickerOnWebSocket(ticker, to);
};

export const unsubscribeFromTicker = (ticker, to) => {
  tickersHandlers.delete(`${ticker}-${to}`);

  unsubscribeFromTickerOnWebSocket(ticker, to);
};

export const loadTickers = async () => {
    const response = await fetch(`${API_ALL_TICKERS}`);

    if (response.ok) {
      const tickers = await response.json();
      return Object.values(tickers.Data);
    }

    return [];
}