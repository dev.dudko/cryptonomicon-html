self.onconnect = function(e) {
  debugger
  const port = e.ports[0];

  port.addEventListener('message', (event) => {
    const workerResult = 'Result: ' + (event.data[0] * event.data[1]);
    port.postMessage(workerResult);
  });

  port.start();
}
