module.exports = {
  "html.format.wrapAttributes": "auto",
  "html.format.wrapLineLength": 120,

  printWidth: 120,
  tabWidth: 4,
  bracketSameLine: true,
  trailingComma: "es5",
  semi: false,
  singleQuote: true,
};